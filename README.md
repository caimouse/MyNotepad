What is Notepad++ ?
===================

MyNotepad is a free (free as in both "free speech" and "free beer") source code
editor and Notepad replacement that supports several programming languages and
natural languages. Running in the MS Windows environment, its use is governed by
[GPL License](LICENSE).

See the [MyNotepad  official site](https://blog.csdn.net/caimouse) for more information.


Build Notepad++
---------------

Please follow [build guide](BUILD.md) to build MyNotepad  from source.


Contribution
------------

Code contribution is welcome. Here are some [rules](CONTRIBUTING.md) that your should follow to make your contribution accepted easily. 


